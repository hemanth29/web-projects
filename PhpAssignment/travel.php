<?php

//Declared Initially for the form to run as we would have echoed this in javascript
$retrievedjsonold = '{"ad":"ad"}';
$jsonData = '{"results" : ""}';
$url = '';

//Function to get the location when location radio button is selected
if(isset($_POST['getLatFromLoc'])){ 
	$sendLoc = urlencode($_POST["getLatFromLoc"]);
	$sendLoc = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?&address=" . $sendLoc . 
	"&key=AIzaSyBVKj4h7H4HOM8WINTzqKw2QeQRRtlPP1g");
	$sendLoc = json_decode($sendLoc,true);
	$sendLoc= $sendLoc["results"]["0"]["geometry"]["location"];
	$sendLoc = json_encode($sendLoc);
	echo $sendLoc;
	die();
}

//Function to process the form input and get the json data of results from NearbySearch API
if(isset($_POST['userID1'])){ 
	// To delete all the old photos saved by old searches.
	$files = glob('/home/scf-02/eshwarap/WebInstallation/apache2/htdocs/photo*'); // get all file names
	foreach($files as $file){ // iterate files
		if(is_file($file))
		unlink($file); // delete file
	}
	$keyword = $_POST["key"];
	$category = $_POST["category"];
	$distance = $_POST["distance"];
	if($_POST["radioloc"] == "location")
	{ 
		$locationName = urlencode($_POST["locationInput"]);
		$locationName = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?&address=" . $locationName . 
		"&key=AIzaSyBVKj4h7H4HOM8WINTzqKw2QeQRRtlPP1g");
		$locationName = json_decode($locationName,true);    
		$lattitude = $locationName["results"]["0"]["geometry"]["location"]["lat"];
		$longitude = $locationName["results"]["0"]["geometry"]["location"]["lng"];
	}
	else
	{
		$lattitude = $_POST["lattitude"];
		$longitude = $_POST["longitude"];
	}
	//Converet distance in miles to meters
	if($distance==NULL || $distance >50000)
	{
		$distance = 10*1609.344;
	}
	else
	{
		$distance = $distance*1609.344;
	}
	$keyword = urlencode($keyword); // Done so as to eliminate spaces given in the keyword
	$concatURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";
	$concatURL = $concatURL . "location="  . $lattitude ."," . $longitude .
	"&radius=" . $distance . "&type=" . $category . "&keyword=" . $keyword . "&key=AIzaSyAyjC6HIfiqCJgD-yTwryp75hAtL17TTcg" ;
	$jsonData = file_get_contents($concatURL);
	echo $jsonData;
	die();  //code ends here and control return to http responsetext function in javascript
}

//Once we have the Json data from Nearby google API , we retrieve the images for the locations using the place ID which is sent from Javascript
if(isset($_POST['userID'])){
	$url = $_POST['userID'];
	$places = "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$url."&key=AIzaSyBdn6lAyh8UWQOkFzDJq1yPWrd8BQbNINg";
	$retrievedjsonold = file_get_contents($places);
	$retrievedjson =  json_decode($retrievedjsonold,true);
	if(isset($retrievedjson["result"]["photos"])){   
		$reseultsforPhotos = $retrievedjson["result"]["photos"];
		for($i=0;$i<count($reseultsforPhotos) && $i<5;$i++){
			$eachphoto[$i] = $reseultsforPhotos[$i]["photo_reference"];
			$eachWidth = $reseultsforPhotos[$i]["width"];
			$eachphotocontent[$i] = file_get_contents( "https://maps.googleapis.com/maps/api/place/photo?maxwidth=".$eachWidth."&photoreference=".$eachphoto[$i] ."&key=AIzaSyCnqSeyLUsaG8tVeOy2usm5omWaAWavJu8");
			file_put_contents("photo".$url.$i.".jpg",$eachphotocontent[$i]); // Save the file as photo + place id + [0-5].jpg
		}
	}
	else{
		$retrievedjsonold = '{"result":{}}'; //if results doesnt contain any images return this dummy so that it can be checked in javascript and displayed accrodingly
	}
	echo $retrievedjsonold; 
	die();  
} 

?>
<!DOCTYPE HTML>
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />
<html>
<head>
<style>
.travelheader
{
	text-align: center;
	
	font-style: italic;
	background-color: #faf9fa;
	padding-top: 10px;
	margin-top: 10px;
	margin-bottom: 10px;
	
}
.travelheaderfrReview
{
	text-align: center;
	font-style: italic;
	padding-top: 10px;
	margin-top: 10px;
	margin-bottom: 10px;
	margin-left: auto;
	margin-right: auto;
	background-color:white;
	border-top: 1px solid black; border-right: 1px solid black;border-left: 1px solid black;
}
.bodytravel
{
	margin-left: 250px;
	margin-right:250px;
	background-color: #faf9fa;
}
.travel
{
	text-align: left;
}
.tableStyle
{
	border: 1px solid black;
	text-align: center;
	margin-left: auto;
	margin-right: auto;
}
.tr { 
	border: solid;
	border-width: 1px 0;
}
.hasTooltip span {
	display: none;
	color: #000;
	text-decoration: none;
	padding: 3px;
	position: absolute;
	border: 1px solid;
}
.tooltiptext
{
	display: none;
	
	width: 180px;
	background-color: white;
	color: black;
	text-align: center;
	border-radius: : 10px;
	padding: 2px 0;
	
	/* Position the tooltip */
	position: absolute;
	z-index: 1;
}


input[type=text], select, textarea {
	
	padding: 5px;
	border: 1px solid #ccc;
	border-radius: 4px;
	box-sizing: border-box;
	margin-top: 6px;
	margin-bottom: 16px;
	resize: vertical;
}

input[type=submit] {
	background-color: #4CAF50;
	color: white;
	padding: 5px 5px;
	border: none;
	border-radius: 4px;
	cursor: pointer;
}



</style>
</head>
<body>
<fieldset class= "bodytravel">
<h1 class = "travelheader"> Travel and Entertainment Search </h1>
<hr>
<form class = "travel" action = "travel.php" method = "post" id = "travelForm" accept-charset="utf-8" onsubmit="return false">
<label for="key"><b> Keyword :<b></label>
<input type="text" style="margin-left: 45px" id="key" name="key" placeholder="" ="required"  value="<?php echo isset($_POST['key']) ? $_POST['key'] : '' ?>" required>
<div>
<label for="category">Category :</label>
<select style="margin-left: 45px; width:150px; height:28px" id ="category" name="category" >
<option value="cafe">cafe</option>
<option value="bakery">bakery</option>
<option value="restaurant">restaurant</option>
<option value="beauty_salon">beauty salon</option> 
<option value="casino">casino</option>
<option value="movie_theater">movie theatre</option>
<option value="lodging">lodging</option>
<option value="airport">airport</option>
<option value="train_station">train station</option>
<option value="subway_station">subway station</option>
<option value="bus_station">bus station</option>
<option value="default" selected=>Default</option>
</select>
</div>
<div>
<label  for="distance"> Distance (miles):</label>
<input style="margin-left: 3px;" type="text" name="distance" id="distance"  placeholder="10" >
<span>from</span>
<input type="radio" id="radiohere" name="radioloc" value="here" required checked="true">
<label for="radiohere">Here</label>
<div style ="margin-left: 340px ;">
<input   type="radio" id="loc" name="radioloc" value="location" required>
<input  type="text" id="locationText" name="locationInput" value="" placeholder="location" style="resize: vertical;"; disabled>
</div>
</div>
<input type="hidden" id="lat" name="lattitude" value="">
<input type="hidden" id="long" name="longitude" value="">
<button style="width:80px;height:30px" type="submit" id = "search" name="submit" value="submit" onclick="submitclick()" > Search</button>
<button style="margin-left: 15px;width:80px;height:30px" onclick="resetall()" id ="clearb" name="clear" value="clear">Clear</button>
</form>
</fieldset>
<div id = "afterform" name ="afterform"></div>
<div id = "afterform1" name ="afterform1"></div>
<div>
<div id="afterform2" name ="afterform"></div>
<div id="reviews" name="reviews"></div>
<div id="afterform3" name="afterform1"></div>
<div id="photos" name="photos"></div>
</div>

<script type="text/javascript">


var reviewsPresent=true;
var photoPresent =true;
var photosdownloaded = false;
var reviewsdownloaded = false;
//
document.getElementById('loc').addEventListener('change',function enableLocationText()
{
	locationText.removeAttribute('disabled');
	locationText.setAttribute('required','required');
},false);  

//radiobutton of here change
document.getElementById('radiohere').addEventListener('change',radioChange,false);
function radioChange(){
	locationText.setAttribute('disabled','disabled');
	var xhttp = new XMLHttpRequest();
	var url = "http://ip-api.com/json";
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			jsonObj = JSON.parse(xhttp.responseText);
			var search = document.getElementById('search');
			search.removeAttribute('disabled'); 
		}
	};
	xhttp.open("GET",url, true);
	xhttp.send();
}

function submitclick(){
	document.getElementById('afterform1').innerHTML ="";
	document.getElementById('afterform').innerHTML ="";
	document.getElementById('afterform2').innerHTML ="";
	document.getElementById('afterform3').innerHTML ="";
	document.getElementById("photos").innerHTML = "";
	document.getElementById("reviews").innerHTML = "";
	if(document.getElementById('key').value==""){
	}
	else if((document.getElementById('locationText').value =="" && document.getElementById('loc').checked==true)){
	}
	else{
		var xhttptry = new XMLHttpRequest();
		var url = "travel.php";
		xhttptry.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200){
				jsonObject = JSON.parse(xhttptry.responseText);
				ds = jsonObject["results"];
				var table ="";
				if(ds.length == 0){   
					table = "<p class = 'travelheader' style='margin-left: 200px;margin-right:200px;'>No Records have been found</p>";
				}
				else{
					table += "<table class='tableStyle'>";
					table += "<tr>";
					table += "<th class='tableStyle'>Category</th>";
					table += "<th class='tableStyle'>Name </th>";
					table += "<th class='tableStyle'>Address</th>";
					table += "</tr>";
					for(i=0;i<ds.length;i++){
						var icon = ds[i]["icon"];
						var name = ds[i]["name"];
						var vicinity = ds[i]["vicinity"];
						var eachLat = ds[i]["geometry"]["location"]["lat"];
						var eachLong = ds[i]["geometry"]["location"]["lng"];
						table += "<tr>";
						table += "<td class='tableStyle'> <img src = '" +icon +"' height=45px></td>";
						table += '<td class="tableStyle" onclick =nameget("'+ ds[i]["place_id"]+ '","'+ i + '");>'+name+'</td>';
						table += '<td class="tableStyle"  ><span class="hasTooltip";><p onclick=initMap('+eachLong+','+eachLat+','+i+')>'+  vicinity+'</p>';
						table += '<span id ="mapDiv'+i+'" ></span>';
						table += '<div><table id="mapTable'+i+'" style="background-color:grey;display: none;color: #000;text-decoration: none;padding: 3px;position: absolute;border: 1px solid;">';
						table += '<tr><td onclick=ride("WALKING")>Walk</td></tr><tr><td onclick=ride("BICYCLING")>Bike</td></tr><tr><td onclick=ride("DRIVING")>Drive</td></tr></table></td>';
						table += "</tr>";
					}
					table += "</table>";
				}
				var currentBody = document.getElementsByTagName('body')[0];
				var newTable = document.getElementById('afterform');
				newTable.innerHTML = table;
				currentBody.appendChild(newTable);
			}
		};
		var temp="";
		if(document.getElementById('loc').checked== true) temp =document.getElementById('locationText').value;
		//Send all the form data to PHP
		xhttptry.open("POST", "travel.php", true);
		xhttptry.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		var sendvalsearch = "userID1="+1+"&key="+document.getElementById('key').value+"&category="+document.getElementById('category').value+
		"&distance="+document.getElementById('distance').value+"&radioloc="+(document.getElementById('loc').checked?"location":"elsehere")+
		"&lattitude="+ document.getElementById('lat').value+"&longitude="+ document.getElementById('long').value+"&locationInput="+
		document.getElementById('locationText').value;
		xhttptry.send(sendvalsearch);
	}
}

window.onload = function(){
	locationText.setAttribute('disabled','disabled');
	var xhttp = new XMLHttpRequest();
	var url = "http://ip-api.com/json";
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			jsonObj = JSON.parse(xhttp.responseText);
			var search = document.getElementById('search');
			search.removeAttribute('disabled'); 
			document.getElementById('lat').value = jsonObj.lat; 
			document.getElementById('long').value = jsonObj.lon;
		}
	};
	xhttp.open("GET",url, true);
	xhttp.send();
}

function resetall(){
	document.getElementById('afterform1').innerHTML ="";
	document.getElementById('afterform').innerHTML ="";
	document.getElementById('afterform2').innerHTML ="";
	document.getElementById('afterform3').innerHTML ="";
	document.getElementById("photos").innerHTML = "";
	document.getElementById("reviews").innerHTML = "";
	document.getElementById("travelForm").reset();
	locationText.setAttribute('disabled','disabled');
}

function nameget(x,index){   
	
	document.getElementById("afterform").innerHTML ="";
	document.getElementById("afterform1").innerHTML ="";
	che = x;
	var createReviewArrow = '<center><p><h1>'+ds[index]["name"]+'</h1></p><center>';
	createReviewArrow += '<center><p id="showrev" >click here to show reviews</p>';
	createReviewArrow += '<img id="arrowReview" src="arrow_down.jpg"  style=" height:10px;width:30px;padding:5px;" onclick="openReviews()"; /></center>';
	document.getElementById("afterform2").innerHTML = createReviewArrow;
	var createPhotoArrow = '<center><p id="showpho" >click here to show Photos</p>';
	createPhotoArrow += '<img id="arrowPhoto" src="arrow_down.jpg" style="height:10px;width:30px;padding:5px;" onclick="openPhotos()"; /></center>';
	document.getElementById("afterform3").innerHTML = createPhotoArrow;
	getphotos();
	getReviews();
	document.getElementById('photos').style.display ='none';
	document.getElementById('reviews').style.display ='none';
}

function getphotos(){
	console.log("Entered get photos");
	var ajx = new XMLHttpRequest();
	ajx.onreadystatechange = function () {
		if (ajx.readyState == 4 && ajx.status == 200) { 
			var photoAndReviewa = JSON.parse(ajx.responseText);
			var photos =  photoAndReviewa["result"]["photos"];
			if(photos==undefined)
			{
				console.log("enter no photos condition");
				document.getElementById("photos").innerHTML ="<center><div style='background-color:lightgrey;height:30px;position:center;width:600px; text-align:center' >Photos not present<div></center>";
				photosdownloaded = true;
			}
			else
			{
				var createPhoto = "<table class = 'travelheaderfrReview'>";
				for(i=0;i<5 && i<photos.length;i++)
				{
					createPhoto += '<tr><td style="text-align:left ;border-bottom: 1px solid black"><a href="photo'+ che +i+ '.jpg" target="_blank"><img id="photocheck' +i+'"   style="height:600px;width:600px;padding:5px;" src="photo'+ che +i+ '.jpg" /></a></td></tr>';
				}
				createPhoto += "</table>";
				document.getElementById("photos").innerHTML = createPhoto;  
				console.log(createPhoto);
				photosdownloaded=true;
				
			}
		}
	};
	ajx.open("POST", "travel.php", true);
	ajx.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var sendval = "userID="+che;
	ajx.send(sendval);
}

function openPhoto(photoID){
	
}

function getReviews(){
	var ajx = new XMLHttpRequest();
	ajx.onreadystatechange = function () {  
		if (ajx.readyState == 4 && ajx.status == 200) {
			var photoAndReview = JSON.parse(ajx.responseText);
			var reviews = photoAndReview["result"]["reviews"];
			if(reviews==undefined){
				document.getElementById("reviews").innerHTML ="<center><div style='background-color:lightgrey;height:30px;position:center;width:600px; text-align:center' >Reviews not present<div><\center>";
				reviewsdownloaded = true;
			}
			else{
				var createReview ="<table class = 'travelheaderfrReview'>"; 
				for(i=0;i<reviews.length && i<5;i++){  
					createReview += "<tr><td><img style='width:40px;height:40px' src='"
					+reviews[i]["profile_photo_url"]+"'/>"+reviews[i]["author_name"] +"</td></tr><tr><td style='text-align:center ;border-bottom: 1px solid black;'>"
					+reviews[i]["text"]+"</td></tr>";
				}
				createReview += "</table>";
				document.getElementById("reviews").innerHTML = createReview;
				reviewsdownloaded = true;
			}
		}
	};
	ajx.open("POST", "travel.php", true);
	ajx.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var sendval = "userID="+che;
	ajx.send(sendval);
}


function openReviews(){   
	if(photosdownloaded!=true && reviewsdownloaded!=true) {
		console.log("loading photos");
		getphotos();
		getReviews();
	}
	if(reviewsPresent!=false){
		if(document.getElementById('reviews').style.display == 'none'){
			document.getElementById('photos').style.display ='none';
			document.getElementById('reviews').style.display ='block';
			document.getElementById('arrowReview').src = 'arrow_up.jpg';
			document.getElementById('arrowPhoto').src = 'arrow_down.jpg';
			document.getElementById('showrev').innerHTML ="click here to hide reviews";
			document.getElementById('showpho').innerHTML ="click here to show photos";
		}
		else{
			document.getElementById('reviews').style.display ='none';
			document.getElementById('arrowReview').src = 'arrow_down.jpg';
			document.getElementById('showrev').innerHTML ="click here to show reviews";
		}
	}
}

function openPhotos(){   
	if(photosdownloaded!=true && reviewsdownloaded!=true){
		console.log("loading photos");
		getphotos();
		getReviews();
	}
	if(photoPresent!=false){
		if(document.getElementById('photos').style.display == 'none'){ 
			document.getElementById('photos').style.display ='block';
			document.getElementById('reviews').style.display ='none';
			document.getElementById('arrowPhoto').src = 'arrow_up.jpg';
			document.getElementById('showpho').innerHTML ="click here to hide photos";
			document.getElementById('showrev').innerHTML ="click here to show reviews";
			console.log(document.getElementById('arrowPhoto').src);
			document.getElementById('arrowReview').src = 'arrow_down.jpg';
		}
		else{
			document.getElementById('showpho').innerHTML ="click here to show photos";
			document.getElementById('photos').style.display ='none';
			document.getElementById('arrowPhoto').src = 'arrow_down.jpg';
		}
	}
}

function initMap(u,v,i){   
	currenti = i;
	if(document.getElementById('mapDiv'+i).style.display == "" || document.getElementById('mapDiv'+i).style.display == 'none'){
		document.getElementById('mapDiv'+i).style.display ='block';
		document.getElementById('mapTable'+i).style.zIndex = '2';
		document.getElementById('mapTable'+i).style.display = 'block';
		document.getElementById('mapDiv'+i).style.height= '400px'; 
		document.getElementById('mapDiv'+i).style.width= '400px'; 
		document.getElementById('mapDiv'+i).style.zIndex = "1";
		forUselat = v;
		forUselong = u;
		directionsService = new google.maps.DirectionsService();
		directionsDisplay = new google.maps.DirectionsRenderer();
		var place = new google.maps.LatLng(forUselat, forUselong);
		var mapOptions = {
			zoom:15,
			center: place
		}
		var map = new google.maps.Map(document.getElementById('mapDiv'+i), mapOptions);
		var marker = new google.maps.Marker({
			position: place,
			map: map
		});
		directionsDisplay.setMap(map);    
	}
	else{
		document.getElementById('mapDiv'+i).style.display ='none';
		document.getElementById('mapTable'+i).style.display ='none';
	}
}

function ride(drivemode){ 
	var temp="";
	var latt ="";
	var long ="";
	if(document.getElementById('loc').checked== true){ 
		temp =document.getElementById('locationText').value;
		var ajx = new XMLHttpRequest();
		ajx.onreadystatechange = function () {
			if (ajx.readyState == 4 && ajx.status == 200) {               
				var receivedLoc = JSON.parse(ajx.responseText);
				latt = receivedLoc["lat"];
				long = receivedLoc["lng"];
				var directionsService = new google.maps.DirectionsService();
				var directionsDisplay = new google.maps.DirectionsRenderer();
				//document.getElementById('mapDiv'+currenti).innerHTML ="";
				var map = new google.maps.Map(document.getElementById('mapDiv'+currenti), {
					zoom:7,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				directionsDisplay.setMap(map);
				var request = {
					origin: {lat:latt , lng: long},
					destination:{lat: forUselat, lng: forUselong},
					travelMode: drivemode
				};
				directionsService.route(request, function(response, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(response);
					}
				}); 
			}
		};
		ajx.open("POST", "travel.php", true);
		ajx.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		var sendval = "getLatFromLoc="+temp;
		ajx.send(sendval);
	}
	else{
		latt = document.getElementById('lat').value;
		long = document.getElementById('long').value;   
		var directionsService = new google.maps.DirectionsService();
		var directionsDisplay = new google.maps.DirectionsRenderer();
		//document.getElementById('mapDiv'+currenti).innerHTML ="";
		var map = new google.maps.Map(document.getElementById('mapDiv'+currenti), {
			zoom:7,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		directionsDisplay.setMap(map);
		var request = {
			origin: {lat: parseFloat(latt) , lng: parseFloat(long)},
			destination:{lat: forUselat, lng: forUselong},
			travelMode: drivemode
		};
		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			}
		}); 
	}
}

</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKOXL_OzAKueNOwH55oE1qq9okmHbYypg"></script>
</body>
</html>